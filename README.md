# Semana 19

### Relaciones N a N y Carro de compras

1. Intro a relaciones N a N
2. Modelos y relaciones
3. Agregando datos
4. Borrando registros y asociaciones
5. Index de peliculas
6. Agregando Tags
7. Verificando el Post
8. Borrado de Tags
9. Verificando el borrado
10. Carro de compras
11. Creando un proyecto con postgresql
12. Instalando Devise
13. Modelo y controller de producto
14. Faker y Seed
15. Vista de productos
16. Modelo de orden de compra
17. Link y ruta anidada para la compra
18. Procesando la orden de compra
19. Listado de ordenes